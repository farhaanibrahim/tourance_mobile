import 'package:flutter/material.dart';
import 'package:tourance_mobile/home.dart';
import 'package:tourance_mobile/splashscreen.dart';

void main() {
  runApp(MaterialApp(
    title: 'Tourance',
    home: Splashscreen(),
    routes: <String, WidgetBuilder>{
      '/home': (BuildContext contex) => Home(),
    },
  ));
}

